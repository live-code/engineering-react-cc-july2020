import React, { Fragment, useEffect, useRef, useState } from 'react';
import Axios from 'axios';
import { User } from '../model/user';


export default function Users() {
  const [users, setUsers] = useState<User[]>([]);
  const [error, setError] = useState<boolean>(false)

  const inputName = useRef<HTMLInputElement>(null);
  const inputSurName = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputName.current?.focus();
    Axios.get<User[]>('http://localhost:3001/users/')
      .then(res => setUsers(res.data))
      .catch(() => setError(true))
  }, []);

  function addUserHandler() {
    setError(false);
    if (inputName.current?.value ) {

      const formData = {
        name: inputName.current.value,
        surname: inputSurName.current?.value || '',
      };

      Axios.post<User>('http://localhost:3001/users/', formData)
        .then(res => setUsers([...users, res.data ]))
        .catch(() => setError(true))

      inputName.current.value = '';
    }
  }

  function keyPressHandler(e: React.KeyboardEvent<HTMLInputElement>) {
    console.log(e.currentTarget.value)
    if (e.key === 'Enter') {
      addUserHandler();
    }
  }

  function deleteHandler(id: number) {
    setError(false);
    Axios.delete('http://localhost:3001/users/' + id)
      .then(() => {
        setUsers(
          users.filter(u => u.id !== id)
        )
      })
      .catch(() => setError(true))
  }

  return (
    <div>
      { error && <div className="alert alert-danger">errore</div> }
      <input type="text" ref={inputName} onKeyPress={keyPressHandler}/>
      <input type="text" ref={inputSurName} onKeyPress={keyPressHandler}/>

      <button onClick={addUserHandler}>Add</button>

      <hr/>

      {
        users.map(u => {
          return <li key={u.id}>
            {u.name} {u.surname} ({u.id})
            <i className="fa fa-trash" onClick={() => deleteHandler(u.id)} />
          </li>
        })
      }
    </div>
  )
}
