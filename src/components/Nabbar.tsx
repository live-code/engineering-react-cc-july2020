import React from 'react';
import { NavLink } from 'react-router-dom';

export const NavBar = () => {
  return (
    <nav className="navbar navbar-expand navbar-light bg-light">
      <a className="navbar-brand" href="#">Navbar</a>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink
              exact={true}
              className="nav-link" activeClassName="activeNav" to="/">Home</NavLink>
          </li>

          <li className="nav-item">
            <NavLink className="nav-link" activeClassName="activeNav" to="/demo-state">State</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" activeClassName="activeNav" to="/users">Users</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" activeClassName="activeNav" to="/users-controlled">Users Controlled</NavLink>
          </li>
          <li className="nav-item">
            <NavLink className="nav-link" activeClassName="activeNav" to="/uikit">UIkit</NavLink>
          </li>
        </ul>
      </div>
    </nav>
  )
}
